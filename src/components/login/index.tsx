import { ChangeEvent, useState } from "react";
import React from "react";
import styles from "./login.module.scss";

export interface InputProps {
  username: string;
  password: string;
}

interface Props {
  onSubmit?: () => void;
}

const Login = ({ onSubmit }: Props) => {
  const [state, setState] = useState<InputProps>({
    username: "",
    password: "",
  });

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.target;
    setState({ ...state, [name as keyof InputProps]: value });
  };

  const submitUser = () => {
    sessionStorage.setItem(
      "data",
      JSON.stringify({ username: state.username })
    );
    onSubmit?.();
  };

  return (
    <div className={styles["login"]}>
      <div className={styles["login__title"]}>Login</div>
      <div className={styles["login__input--wrapper"]}>
        <input
          placeholder="Username"
          name="username"
          value={state.username}
          onChange={handleChange}
        />
      </div>
      <div className={styles["login__input--wrapper"]}>
        <input
          placeholder="Password"
          name="password"
          value={state.password}
          onChange={handleChange}
        />
      </div>
      <div className={styles["login__submit"]}>
        <button
          disabled={!state.username || !state.password}
          onClick={submitUser}
        >
          Sign In
        </button>
      </div>
    </div>
  );
};

export default Login;
