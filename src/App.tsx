import React from "react";
import ReactDOM from "react-dom";
import Login from "./components/login";

import "./index.scss";

const App = () => <Login />;

ReactDOM.render(<App />, document.getElementById("authRoot"));
